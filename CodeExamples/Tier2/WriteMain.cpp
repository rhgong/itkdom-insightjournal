
// This user program produces a test object, and then write it to an
// XML file using the writer described above.

#include "DOMTestObjectDOMWriter.h"

int main( int argc, char* argv[] )
{
  if ( argc < 2 )
    {
    std::cout << "Expect an output XML file!" << std::endl;
    return EXIT_FAILURE;
    }

  // Generate the test object ...
  DOMTestObject::Pointer inputObject = DOMTestObject::New();

  // Write the test object to an XML file.
  const char* outputXMLFileName = argv[1];
  DOMTestObjectDOMWriter::Pointer writer = DOMTestObjectDOMWriter::New();
  writer->SetInput( inputObject );
  writer->SetFileName( outputXMLFileName );
  writer->Update();

  return EXIT_SUCCESS;
}
