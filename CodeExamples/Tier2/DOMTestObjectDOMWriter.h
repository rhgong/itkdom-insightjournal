#ifndef __DOMTestObjectDOMWriter_h
#define __DOMTestObjectDOMWriter_h

// The writer derives from itk::DOMWriter<T> and, except for the common definitions
// required by itk::Object, it needs only to implement one protected virtual function, GenerateData.

#include <itkDOMWriter.h>
#include "DOMTestObject.h"

class DOMTestObjectDOMWriter : public itk::DOMWriter<DOMTestObject>
{
public:
  /** Standard class typedefs. */
  typedef DOMTestObjectDOMWriter      Self;
  typedef itk::DOMWriter<DOMTestObject>    Superclass;
  typedef itk::SmartPointer< Self >        Pointer;
  typedef itk::SmartPointer< const Self >  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(DOMTestObjectDOMWriter, itk::DOMWriter);

protected:
  DOMTestObjectDOMWriter() {}

private:
  DOMTestObjectDOMWriter(const Self &); //purposely not implemented
  void operator=(const Self &); //purposely not implemented
  
protected:
  virtual void GenerateData( DOMNodeType* outputdom, const void* ) const
  {
    // First set the tag name for the intermediate DOM object.
    outputdom->SetName( "DOMTestObject" );

    // Retrieve the test object to be written out.
    const InputType* input = this->GetInput();

    // We will use the itk::FancyString to facilitate data writing to string.
    itk::FancyString s;

    // Create a child node with the tag name "foo", and add it to the DOM object.
    DOMNodePointer node = DOMNodeType::New();
    node->SetName( "foo" );
    outputdom->AddChild( node );

    // Finally retrieve the foo value from the input test object, convert it
    // to a string, and set the value for the attribute "value" in the newly created
    // child node.
    float fooValue = input->GetFooValue();
    s << fooValue;
    node->SetAttribute( "value", s );
  }
};

#endif // __itkDOMTestObjectDOMWriter_h
