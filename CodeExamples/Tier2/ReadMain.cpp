
// This user program reads a test object from an XML file using the reader
// described above, and then performs subsequent processing.

#include "DOMTestObjectDOMReader.h"

int main( int argc, char* argv[] )
{
  if ( argc < 2 )
    {
    std::cout << "Expect an input XML file!" << std::endl;
    return EXIT_FAILURE;
    }

  // Variable to store the output test object.
  DOMTestObject::Pointer outputObject;

  // Read the object from an XML file.
  const char* inputXMLFileName = argv[1];
  DOMTestObjectDOMReader::Pointer reader = DOMTestObjectDOMReader::New();
  reader->SetFileName( inputXMLFileName );
  reader->Update();
  outputObject = reader->GetOutput();

  // Perform subsequent processing with the newly read object ...
  
  return EXIT_SUCCESS;
}
