
#ifndef __DOMTestObject_h
#define __DOMTestObject_h

#include <itkObject.h>
#include <string>

class DOMTestObject : public itk::Object
{
public:
  /** Standard class typedefs. */
  typedef DOMTestObject                    Self;
  typedef itk::Object                      Superclass;
  typedef itk::SmartPointer< Self >        Pointer;
  typedef itk::SmartPointer< const Self >  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(DOMTestObject, itk::Object);

  /** Functions to set/get foo value. */
  itkSetMacro( FooValue, float );
  itkGetConstMacro( FooValue, float );

private:
  DOMTestObject() : m_FooValue(0) {}

  DOMTestObject(const Self &); //purposely not implemented
  void operator=(const Self &); //purposely not implemented

  float m_FooValue;
};

#endif // __DOMTestObject_h
