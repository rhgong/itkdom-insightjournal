#ifndef __DOMTestObjectDOMReader_h
#define __DOMTestObjectDOMReader_h

// The reader derives from itk::DOMReader<T> and, except for the common definitions
// required by itk::Object, it needs only to implement one protected virtual function, GenerateData.

#include <itkDOMReader.h>
#include "DOMTestObject.h"

class DOMTestObjectDOMReader : public itk::DOMReader<DOMTestObject>
{
public:
  /** Standard class typedefs. */
  typedef DOMTestObjectDOMReader           Self;
  typedef itk::DOMReader<DOMTestObject>    Superclass;
  typedef itk::SmartPointer< Self >        Pointer;
  typedef itk::SmartPointer< const Self >  ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(DOMTestObjectDOMReader, itk::DOMReader);

protected:
  DOMTestObjectDOMReader() {}

private:
  DOMTestObjectDOMReader(const Self &); //purposely not implemented
  void operator=(const Self &); //purposely not implemented
  
protected:
  virtual void GenerateData( const DOMNodeType* inputdom, const void* )
  {
    // First check whether the user has supplied a correct XML document.
    if ( inputdom->GetName() != "DOMTestObject" )
      {
      itkExceptionMacro( "tag name DOMTestObject is expected" );
      }

    // The user may have already provided an instance of the test object
    // as the output. If so, retrieve this object for subsequent reading.
    OutputType* output = this->GetOutput();

    // If the user hasn't provided an instance as the output, create one.
    if ( output == NULL )
      {
      OutputType::Pointer object = OutputType::New();
      output = (OutputType*)object;
      this->SetOutput( output );
      }

    // We will use the itk::FancyString to facilitate data reading from string.
    itk::FancyString s;

    // Retrieve the child node with the tag name "foo".
    const DOMNodeType* node = inputdom->GetChild( "foo" );
    if ( node == NULL )
      {
      itkExceptionMacro( "Child foo not found!" );
      }

    // Now retrieve the value of the attribute "value", which is a text string,
    // and convert it to type float.
    float fooValue = 0;
    s = node->GetAttribute( "value" );
    s >> fooValue;

    // Finally assign the obtained value to the output object.
    output->SetFooValue( fooValue );
  }
};

#endif // __DOMTestObjectDOMReader_h
