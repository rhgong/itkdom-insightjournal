#include <itkDOMNodeXMLReader.h>

int main( int argc, char* argv[] )
{
  if ( argc < 2 )
    {
    std::cout << "Expect an input XML file!" << std::endl;
    return EXIT_FAILURE;
    }
    
  /////////////////////////////////////////////////////////
  // Tier 1 reading: first read a DOM object from the XML file,
  // then extract the information of interest from the DOM object.
  /////////////////////////////////////////////////////////

  // Step 1: read the DOM object from an XML file
  itk::DOMNode::Pointer outputDOMObject;
  const char* inputXMLFileName = argv[1];
  itk::DOMNodeXMLReader::Pointer reader = itk::DOMNodeXMLReader::New();
  reader->SetFileName( inputXMLFileName );
  reader->Update();
  outputDOMObject = reader->GetOutput();

  // Step 2: read the variables from the DOM object
  if ( outputDOMObject->GetName() != "my_settings" )
    {
    throw "Unrecognized input XML document!";
    }
  std::string desc = "";
    {
    itk::DOMNode* node = outputDOMObject->GetChild( "desc" );
    desc = node->GetTextChild()->GetText();
    }
  double score = 0.0;
    {
    itk::DOMNode* node = outputDOMObject->GetChild( "score" );
    itk::FancyString fs = node->GetAttribute( "value" );
    fs >> score;
    }

  // Perform subsequent processing with the newly read information ...
  
  return EXIT_SUCCESS;
}
