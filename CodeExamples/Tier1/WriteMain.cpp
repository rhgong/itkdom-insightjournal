#include <itkDOMNodeXMLWriter.h>

int main( int argc, char* argv[] )
{
  if ( argc < 2 )
    {
    std::cout << "Expect an output XML file!" << std::endl;
    return EXIT_FAILURE;
    }

  /////////////////////////////////////////////////////////
  // Tier 1 writing: first write the information of interest to a DOM object,
  // then write the DOM object to an XML file.
  /////////////////////////////////////////////////////////
  
  // Step 1: write the variables to a DOM object
  itk::DOMNode::Pointer inputDOMObject = itk::DOMNode::New();
  inputDOMObject->SetName( "my_settings" );
  std::string desc = "Hello World!";
    {
    // create a node and add it to the DOM object
    itk::DOMNode::Pointer node = itk::DOMNode::New();
    node->SetName( "desc" );
    inputDOMObject->AddChildAtEnd( node );
    // add a text child to the newly created node
    node->AddTextChild( desc );
    }
  double score = 0.1;
    {
    // create a node and add it to the DOM object
    itk::DOMNode::Pointer node = itk::DOMNode::New();
    node->SetName( "score" );
    inputDOMObject->AddChildAtEnd( node );
    // add an attribute to the newly created node
    itk::FancyString fs;
    fs << score;
    node->SetAttribute( "value", fs );
    }

  // Step 2: write the DOM object to an XML file
  const char* outputXMLFileName = argv[1];
  itk::DOMNodeXMLWriter::Pointer writer = itk::DOMNodeXMLWriter::New();
  writer->SetInput( inputDOMObject );
  writer->SetFileName( outputXMLFileName );
  writer->Update();

  return EXIT_SUCCESS;
}
