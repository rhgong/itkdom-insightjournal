#include <itkFancyString.h>
#include <vector>
#include <itkArray.h>

int main( int argc, char* argv[] )
{
  itk::FancyString fs;

  // Write a fundamental C data to the string.
  int i = 100;
  fs << itk::ClearContent << i;
  // Read a fundamental C data from the string.
  fs >> i;

  // Write a vector to the string.
  std::vector<float> v( 10, 0.9f );
  fs << itk::ClearContent << v;
  // Read all elements in the string to a vector.
  fs >> v;
  // Read a specified number of elements from the string to a vector.
  v.resize( 3 );
  fs.ToData( v );

  // Write an ITK array to the string.
  itk::Array<double> a( 10 );
    {
    a.Fill( 0.5 );
    }
  fs << itk::ClearContent << a;
  // Read all elements in the string to an ITK array.
  fs >> a;
  // Read a specified number of elements from the string to an ITK array.
  a.SetSize( 5 );
  fs.ToData( a );
  
  return EXIT_SUCCESS;
}
