#include <itkFileTools.h>
#include <fstream>

int main( int argc, char* argv[] )
{
  // We want to write some data to this file, which is located in a directory that does
  // not exist.
  const char* fn = "/sdgfas/de-afd/data.txt";

  // Create the directory as well as the file.
  itk::FileTools::CreateFile( fn );

  // Open the file for writing.
  std::ofstream ofs( fn );
  if ( !ofs.is_open() )
    {
    std::cout << "Cannot write file!" << std::endl;
    }

  // Write the data to the file ...
  
  return EXIT_SUCCESS;
}
