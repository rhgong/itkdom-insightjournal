cmake_minimum_required(VERSION 2.8)

project(DOMCodeExamples)

find_package(ITK REQUIRED)
include(${ITK_USE_FILE})

add_subdirectory(Tier1)
add_subdirectory(Tier2)
add_subdirectory(Utilities)
